Source: heaptrack
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Anton Gladky <gladk@debian.org>
Section: devel
Priority: optional
Build-Depends: cmake,
               debhelper-compat (= 13),
               elfutils,
               kf6-extra-cmake-modules,
               kf6-kio-dev,
               kf6-kconfigwidgets-dev,
               kf6-threadweaver-dev,
               kf6-kitemmodels-dev,
               kf6-ki18n-dev,
               libboost-iostreams-dev,
               libboost-filesystem-dev,
               libboost-system-dev,
               libboost-program-options-dev,
               libboost-regex-dev,
               libdw-dev,
               libdwarf-dev,
               libelf-dev,
               libkchart6-dev,
               libsparsehash-dev,
               libunwind-dev,
               libzstd-dev,
               qt6-base-dev,
               zlib1g-dev,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/heaptrack
Vcs-Git: https://salsa.debian.org/science-team/heaptrack.git
Homepage: https://github.com/KDE/heaptrack

Package: heaptrack
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libheaptrack (= ${binary:Version})
Description: heap memory profiler for Linux
 Heap memory usage profiler. It uses LD_PRELOAD to track all calls
 to the core memory allocation functions and logs these occurrences.
 Additionally, backtraces are obtained and logged. It can also
 generate a historigram of allocation sizes over the number of
 calls. Heaptrack measures the following:
 .
   * Heap memory consumption (like Massif).
   * Number of calls to allocation functions (like callgrind).
   * Total amount of memory allocated, ignoring deallocations.
   * Leaked memory (like memcheck).
 .
 Heaptrack is notable for it's ability to attach to running processes,
 for consuming substantially less memory than Valgrind, and for not
 reducing an application's interactivity as much as Valgrind does.
 Heaptrack is useful for debugging memory leaks and memory ballooning.
 .
 The package contains the command line tools.

Package: heaptrack-gui
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: heap memory profiler for Linux
 Heap memory usage profiler. It uses LD_PRELOAD to track all calls
 to the core memory allocation functions and logs these occurrences.
 Additionally, backtraces are obtained and logged. It can also
 generate a historigram of allocation sizes over the number of
 calls. Heaptrack measures the following:
 .
   * Heap memory consumption (like Massif).
   * Number of calls to allocation functions (like callgrind).
   * Total amount of memory allocated, ignoring deallocations.
   * Leaked memory (like memcheck).
 .
 Heaptrack is notable for it's ability to attach to running processes,
 for consuming substantially less memory than Valgrind, and for not
 reducing an application's interactivity as much as Valgrind does.
 Heaptrack is useful for debugging memory leaks and memory ballooning.
 .
 The package contains the GUI for data analysis.

Package: libheaptrack
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: heap memory profiler for Linux
 Heap memory usage profiler. It uses LD_PRELOAD to track all calls
 to the core memory allocation functions and logs these occurrences.
 Additionally, backtraces are obtained and logged. It can also
 generate a historigram of allocation sizes over the number of
 calls. Heaptrack measures the following:
 .
   * Heap memory consumption (like Massif).
   * Number of calls to allocation functions (like callgrind).
   * Total amount of memory allocated, ignoring deallocations.
   * Leaked memory (like memcheck).
 .
 Heaptrack is notable for it's ability to attach to running processes,
 for consuming substantially less memory than Valgrind, and for not
 reducing an application's interactivity as much as Valgrind does.
 Heaptrack is useful for debugging memory leaks and memory ballooning.
 .
 The package contains the shared libraries.
