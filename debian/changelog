heaptrack (1.5.0-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 03 Oct 2023 19:18:07 +0000

heaptrack (1.4.0-0neon) focal; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 20 Oct 2022 19:18:08 +0000

heaptrack (1.3.0-1) unstable; urgency=medium

  [ Pino Toscano ]
  * [3904e08] Add upstream GPG signing key
  * [856ed9d] Switch watch file to the canonical tarballs
  * [f99d75c] Remove d/scripts/get_orig_src.sh, no more needed now

  [ Anton Gladky ]
  * [d32ea41] New upstream version 1.3.0
  * [fa46f6b] Update patch
  * [be7a8fa] Update gitlab-ci
  * [025cb91] Bump debhelper from old 12 to 13.
  * [6c2a01a] Set upstream metadata fields: Repository, Repository-Browse.
  * [d0f8508] Update standards version to 4.6.0, no changes needed.

 -- Anton Gladky <gladk@debian.org>  Wed, 29 Dec 2021 20:09:05 +0100

heaptrack (1.2.0-1) unstable; urgency=medium

  * [f3ec34f] New upstream version 1.2.0
  * [a42d5a3] Remove unreliable patches
  * [9373ca1] Ignore quilt dir
  * [2322fe2] Bump debhelper from old 11 to 12.
  * [5479da2] Set debhelper-compat version in Build-Depends.
  * [9ba9600] Use canonical URL in Vcs-Browser.
  * [306c516] Set Standards-Version 4.5.0. No changes

 -- Anton Gladky <gladk@debian.org>  Fri, 20 Nov 2020 22:05:40 +0100

heaptrack (1.1.0+20180922.gitf752536-4) unstable; urgency=medium

  [ Nicholas D Steeves ]
  * [58b38ee] Update description to make heaptrack more discoverable to users.
              (Closes: #915241)

 -- Anton Gladky <gladk@debian.org>  Wed, 27 Feb 2019 09:44:39 +0200

heaptrack (1.1.0+20180922.gitf752536-3) unstable; urgency=medium

  * [0b640ca] Fix broken patch

 -- Anton Gladky <gladk@debian.org>  Sat, 29 Sep 2018 15:27:56 +0200

heaptrack (1.1.0+20180922.gitf752536-2) unstable; urgency=medium

  * [143f6cc] Disable some more tests

 -- Anton Gladky <gladk@debian.org>  Sat, 29 Sep 2018 14:56:15 +0200

heaptrack (1.1.0+20180922.gitf752536-1) unstable; urgency=medium

  * [1197d65] New upstream version 1.1.0+20180922.gitf752536. (Closes: #902200)
  * [7f2a7ba] Disable some failing tests for a while
  * [fc4a172] Fix spelling error
  * [0b1ee0e] Set Standards-Version: 4.2.1

 -- Anton Gladky <gladk@debian.org>  Sat, 29 Sep 2018 10:00:48 +0200

heaptrack (1.1.0-3) unstable; urgency=medium

  * [039b42f] Disable two failing tests.

 -- Anton Gladky <gladk@debian.org>  Thu, 14 Jun 2018 18:37:22 +0200

heaptrack (1.1.0-2) unstable; urgency=medium

  * [31bf7e2] Add libzstd-dev to build-depends
  * [db25410] Add libboost-filesystem-dev and libboost-system-dev
              to build-depends
  * [e2bf4ab] Enable all tests to check them on all archs

 -- Anton Gladky <gladk@debian.org>  Wed, 13 Jun 2018 23:56:20 +0200

heaptrack (1.1.0-1) unstable; urgency=medium

  * [ed3605b] Update script for the packing the source
  * [39ada17] New upstream version 1.1.0. (Closes: #901049)
  * [1ea6a50] Set Standards-Version to 4.1.4
  * [640f04e] Update changelog.

 -- Anton Gladky <gladk@debian.org>  Tue, 12 Jun 2018 21:19:13 +0200

heaptrack (1.0.1~20180129.gita4534d5-1) unstable; urgency=medium

  * [a6dc2c1] New upstream version 0~20180129.gita4534d5. (Closes: #887757)
  * [718fc77] Disable KDE test
  * [2e67083] Ignore quilt dir
  * [29e6221] Apply cme fix dpkg
  * [d2703ab] Remove testsuite-field in d/control
  * [429a120] Update VCS-field (move to salsa)
  * [3c1ec4b] Use compat-level 11
  * [e8b0aa0] Update autopkgtest
  * [810d33c] Add one more autopkgtest

 -- Anton Gladky <gladk@debian.org>  Sat, 03 Feb 2018 20:36:37 +0100

heaptrack (1.0.1~20170503.git4da8c45-3) unstable; urgency=medium

  * [dfbead4] Allow stderr output in autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Sun, 07 May 2017 23:12:12 +0200

heaptrack (1.0.1~20170503.git4da8c45-2) unstable; urgency=medium

  * [50e65ec] Disable unittests on mips due to crash in a third lib.
              (Closes: #861826)
  * [f7f6bed] Add d/watch.
  * [84c5439] Add simple autopkgtest.

 -- Anton Gladky <gladk@debian.org>  Sun, 07 May 2017 19:16:18 +0200

heaptrack (1.0.1~20170503.git4da8c45-1) unstable; urgency=medium

  [ Milian Wolff ]
  * [3977193] Fix unittests due to libunwind. (Closes: #860949)

  [ Anton Gladky ]
  * [80b8ee3] Do not override failing of unittests.
  * [6ef6b77] Ignore quilt dir
  * [56de1cc] Do not build in Debug-mode.
  * [ee63343] Add script to download heaptrack from github.
  * [8dd4054] New upstream version 1.0.1~20170503.git4da8c45

 -- Anton Gladky <gladk@debian.org>  Wed, 03 May 2017 22:32:13 +0200

heaptrack (1.0.0-4) unstable; urgency=medium

  * [aaf8b5c] Run unittests during the build.

 -- Anton Gladky <gladk@debian.org>  Sat, 22 Apr 2017 15:07:26 +0200

heaptrack (1.0.0-3) unstable; urgency=medium

  [ Pino Toscano ]
  * [446b623] Optimize build dependencies. (Closes: #859745)

 -- Anton Gladky <gladk@debian.org>  Sun, 09 Apr 2017 00:15:16 +0200

heaptrack (1.0.0-2) unstable; urgency=medium

  * [cf6520a] Add kio-dev as a build-dependency.
  * [8879be4] Add libheaptrack as a dependency of (Closes: #859512)

 -- Anton Gladky <gladk@debian.org>  Tue, 04 Apr 2017 18:09:48 +0200

heaptrack (1.0.0-1) unstable; urgency=medium

  * Initial upload. (Closes: #858858)

 -- Anton Gladky <gladk@debian.org>  Fri, 20 Jan 2017 20:14:59 +0100
